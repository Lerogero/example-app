@extends('layouts.main')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Groups</h1>
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($groups as $group)
                    <tr>
                        <th scope="row">{{$group->name}}</th>
                        <td>{{$group->description}}</td>
                        <td><a href="groups/{{$group->id}}/edit" class="btn btn-primary">Edit</a></td>
                        <td>
                            <form method="post" action="/groups/{{$group->id}}">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <a href="/groups/create" class="btn btn-success">Create Group</a>
        </div>
    </div>
</div>

