<?php

namespace App\Repositories;

use App\Group;
use App\Http\Requests\StoreGroup;
use App\Repositories\Contracts\GroupRepositoryInterface;


class GroupRepository implements GroupRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        return Group::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return Group::findOrFail($id);
    }

    /**
     * @param $name
     * @param $description
     */
    public function create($name, $description)
    {
        Group::create([
            'name' => $name,
            'description' => $description
        ]);
    }

    public function update($id, StoreGroup $request)
    {
        $group = $this->find($id);

        $group->name = $request['name'];
        $group->description = $request['description'];

        $group->save();
    }

    public function delete($id)
    {
        Group::destroy($id);
    }
}