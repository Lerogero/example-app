@extends('layouts.main')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Edit {{$group->name}} Group</h1>
            @include('messages.error')
            <form method="post" action="/groups/{{$group->id}}">
                <div class="form-group">
                    {{csrf_field()}}
                    {{method_field('PATCH')}}

                    <div class="form-group">
                        <label for="name">Group Name</label>
                        <input class="form-control" name="name" value="{{$group->name}}">
                    </div>

                    <div class="form-group">
                        <label for="description">Group Description</label>
                        <input type="field" class="form-control" name="description" value="{{$group->description}}">
                    </div>

                </div>
                <input type="submit" class="btn btn-primary" value="submit">
            </form>
            <a href="/groups" class="btn btn-info">Back</a>
        </div>
    </div>
</div>
