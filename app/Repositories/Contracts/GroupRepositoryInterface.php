<?php


namespace App\Repositories\Contracts;

use App\Http\Requests\StoreGroup;

interface GroupRepositoryInterface
{
    public function getAll();
    public function find($id);
    public function create($name, $description);
    public function update($id, StoreGroup $request);
    public function delete($id);
}