<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['name', 'description'];

    /**
     * get a string link for group
     *
     * @return string
     */
    public function link()
    {
        return "/groups/{$this->id}";
    }
}
