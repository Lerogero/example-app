@extends('layouts.main')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>{{$group->name}}</h1>
            {{$group->description}}
            <div class="form-group">
                <a href="/groups/{{$group->id}}/edit" class="btn btn-primary">Edit</a>
                <form method="post" action="/groups/{{$group->id}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <input type="submit" class="btn btn-danger" value="Delete">
                </form>
            </div>
        </div>
    </div>
</div>
