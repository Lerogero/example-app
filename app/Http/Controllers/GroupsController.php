<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGroup;
use App\Jobs\MakeGroup;
use App\Repositories\Contracts\GroupRepositoryInterface;

/**
 * Class GroupsController
 * @package App\Http\Controllers
 */
class GroupsController extends Controller
{
    /**
     * @var
     */
    public $group;

    /**
     * GroupsController  constructor.
     * @param $group
     */
    public function __construct(GroupRepositoryInterface $group)
    {
        $this->group = $group;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = $this->group->getAll();

        return view('group.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreGroup $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGroup $request)
    {
        dispatch(new MakeGroup($request));

        return redirect('/groups')->with('flash', 'Group Creating');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = $this->group->find($id);

        return view('group.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = $this->group->find($id);

        return view('group.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $id
     * @param \App\Http\Requests\StoreGroup $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, StoreGroup $request)
    {
        $this->group->update($id, $request);

        return redirect('/groups/' . $id . '/edit')->with('flash', 'Group Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->group->delete($id);

        return redirect('/groups')->with('flash', 'Group Deleted');
    }
}
