@extends('layouts.main')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Create Group</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="/groups">
                <div class="form-group">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="name">Group Name</label>
                        <input class="form-control" name="name"">
                    </div>

                    <div class="form-group">
                        <label for="description">Group Description</label>
                        <input type="field" class="form-control" name="description">
                    </div>

                </div>
                <input type="submit" class="btn btn-primary" value="submit">
                <a href="/groups" class="btn btn-info">Back</a>
            </form>
        </div>
    </div>
</div>
