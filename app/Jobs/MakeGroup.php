<?php

namespace App\Jobs;

use App\Http\Requests\StoreGroup;
use App\Repositories\Contracts\GroupRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class MakeGroup implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $description;

    /**
     * Create a new job instance.
     * @param $request
     */
    public function __construct(StoreGroup $request)
    {
        $this->name = $request['name'];
        $this->description = $request['description'];
    }

    /**
     * Execute the job.
     * @param GroupRepositoryInterface $group
     * @return void
     */
    public function handle(GroupRepositoryInterface $group)
    {
        $group->create($this->name, $this->description);
    }
}
